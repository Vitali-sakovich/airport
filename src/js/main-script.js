


function initMap() {
var mapOptions = {
scrollwheel: false,
zoom: 5,
disableDefaultUI: true,
zoomControl: true,
center: new google.maps.LatLng(71.964107, 102.440602),
};
var mapElement = document.getElementById('map');
var map = new google.maps.Map(mapElement, mapOptions);
var marker = new google.maps.Marker({
position: new google.maps.LatLng(71.964107, 102.440602),
map: map
});
}

$(function() {
	
    $('#date').datepicker($.datepicker.regional["ru"]);
    			
});


$('[data-modal=modal-branch]').click(function(){
    $('.modal').addClass('modal-open');
  });
  
  $('.modal__block-close, .modal').click(function(){
    $('.modal').removeClass('modal-open');
  });